import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    theme: "white",
};

const themeSlice = createSlice({
    name: "theme",
    initialState,
    reducers: {
        themeWhite: (state) => {
            state.theme = "white";
        },
        themeBlack: (state) => {
            state.theme = "black";
        },
    },
});

export const { themeWhite, themeBlack } = themeSlice.actions;

export default themeSlice.reducer;

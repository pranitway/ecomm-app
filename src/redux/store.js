import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import logger from "redux-logger";

import themeReducer from "./themeSlice";
import cartReducer from "./cartSlice";
import booksReducer from "./booksSlice";

const store = configureStore({
    reducer: {
        theme: themeReducer,
        cart: cartReducer,
        books: booksReducer,
    },
    middleware: (getDefaultMiddleware) => [...getDefaultMiddleware(), logger],
    devTools: true,
});

export default store;

import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    booksCollection: [
        {
            id: 1,
            name: "abc",
            author: "lorem ipsum",
            description: "lorem ipsum sner let hgt iuftd hsfuj",
            rating: 3,
        },
        {
            id: 2,
            name: "xyz",
            author: "taylor ipsum",
            description: "lorem ipsum sner let hgt iuftd hsfuj",
            rating: 3,
        },
        {
            id: 3,
            name: "pqr",
            author: "lorem gibsum",
            description: "lorem ipsum sner let hgt iuftd hsfuj",
            rating: 3,
        },
    ],
    cart: [],
};

const bookStoreSlice = createSlice({
    name: "cart",
    initialState,
    reducers: {
        addToCart: (state, action) => {
            const index = state.booksCollection.find((book) => {
                return book.id === action.payload.id;
            });

            state.cart.push({
                ...state.booksCollection[index],
                quantity: 0,
                sr_num: state.cart.length + 1,
            });
        },

        removeFromCart: (state, action) => {
            const findIndex = state.booksCollection.findIndex((book) => {
                return book.id === action.payload.id;
            });

            state.cart.splice(findIndex, 1);
        },

        incrementQuantity: (state, action) => {
            // const index = state.booksCollection.find((book) => {
            //     return book.id === action.payload.id;
            // });
            // state.cart[index].quantity = 1;
            state.cart[action.payload.sr_num].quantity += 1;
        },

        decrementQuantity: (state, action) => {
            state.cart[action.payload.sr_num].quantity -= 1;
        },
    },
});

export const {
    addToCart,
    removeFromCart,
    incrementQuantity,
    decrementQuantity,
} = bookStoreSlice.actions;

export default bookStoreSlice.reducer;

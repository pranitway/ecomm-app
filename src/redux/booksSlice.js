import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

// import { useDispatch } from "react-redux";

const initialState = {
    bookCollection: [
        // {
        //     id: 1,
        //     name: "abc",
        //     author: "lorem ipsum",
        //     description: "lorem ipsum sner let hgt iuftd hsfuj",
        //     rating: 3,
        // },
        // {
        //     id: 2,
        //     name: "xyz",
        //     author: "taylor ipsum",
        //     description: "lorem ipsum sner let hgt iuftd hsfuj",
        //     rating: 3,
        // },
        // {
        //     id: 3,
        //     name: "pqr",
        //     author: "lorem gibsum",
        //     description: "lorem ipsum sner let hgt iuftd hsfuj",
        //     rating: 3,
        // },
    ],

    error: "",
    loading: false,
};

// Asynchronous requests created with createAsyncThunk accept three parameters: an action type string, a callback function (referred to as a payloadCreator), and an options object.
export const fetchBooks = createAsyncThunk(
    "allBooks/fetchBooks",
    async (arg, thunkApi) => {
        try {
            const response = await axios.get(
                "https://gist.githubusercontent.com/pranitway/50c1c1fb51e0b8838e37b8796bf6cec8/raw/dd10009db1204febbb742c693ab651f4ff65eabd/users-data"
            );
            console.log(response);
            return response.data;
        } catch (err) {
            return thunkApi.rejectWithValue([], err);
        }
    }
);
// console.log(fetchBooks);
const booksSlice = createSlice({
    name: "allBooks",
    initialState,
    // this reducer will handle the synchronous request
    reducers: {},
    // we use this extra reducer to call the api, this will handle the asynchronous request
    extraReducers: (builder) => {
        builder.addCase(fetchBooks.pending, (state, action) => {
            state.loading = true;
        });
        builder.addCase(fetchBooks.fulfilled, (state, action) => {
            state.bookCollection = action.payload;
            state.loading = true;
        });

        builder.addCase(fetchBooks.rejected, (state, action) => {
            state.error = action.error;
        });
    },
});

// dispatch(fetchBooks());

export default booksSlice.reducer;

import logo from "./logo.svg";
import "./App.css";
import ThemeCheck from "./components/ThemeCheck";
import { Provider } from "react-redux";
import store from "./redux/store";


function App() {
    return (
        <div className="text-center">
            <Provider store={store}>
                <ThemeCheck></ThemeCheck>
            </Provider>
        </div>
    );
}

export default App;

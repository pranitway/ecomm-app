import React from "react";

import { useDispatch, useSelector } from "react-redux";
import { themeWhite, themeBlack } from "../redux/themeSlice";
import { useEffect, useState } from "react";

import { fetchBooks } from "./../redux/booksSlice";

const ThemeCheck = () => {
    let themeState = useSelector((state) => {
        return state.theme;
    });

    const dispatch = useDispatch();

    useEffect(() => {
        console.log("Fetch Action:", fetchBooks());
        dispatch(fetchBooks());
    }, []);

    // function checkTheme() {
    //     console.log(themeState.theme);
    //     let the =
    //         themeState.theme === "white" ? "btn btn-dark" : "btn btn-light";
    //     console.log(the);
    // }
    const [showBox, setShowBox] = useState(false);
    return (
        <div
            className={
                themeState.theme === "white"
                    ? "bg-light text-dark"
                    : "bg-dark text-light"
            }
            style={{ height: "100vh" }}
        >
            <h1>{themeState.theme}</h1>
            <div class="" style={{ width: "100px", margin: "0 auto" }}>
                <button
                    className={
                        themeState.theme === "white"
                            ? "btn btn-dark"
                            : "btn btn-light"
                    }
                    onClick={() => {
                        themeState.theme === "white"
                            ? dispatch(themeBlack())
                            : dispatch(themeWhite());
                    }}
                    onMouseEnter={() => {
                        setShowBox(true);
                    }}
                    onMouseOut={() => {
                        setShowBox(false);
                    }}
                >
                    Change
                </button>

                {showBox && (
                    <ul
                        class="p-2 mt-2 bg-dark text-light"
                        style={{
                            listStyle: "none",
                            border: "1px solid black",
                            borderRadius: "5px",
                        }}
                    >
                        <li style={{ textSize: "15px" }}>ABC</li>
                        <li style={{ textSize: "15px" }}>PQR</li>
                        <li style={{ textSize: "15px" }}>XYZ</li>
                    </ul>
                )}
            </div>
        </div>
    );
};

export default ThemeCheck;
